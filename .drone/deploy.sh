#!/bin/bash

set -e
#

##--------------------------------------------------------------
# for Shipyard API

#curl -X POST -H 'X-Access-Token:user:$2a$10$Pp6bdgNtf0e.9QooaQ5YbOiD0IZmpkBxWCtfR64lQpj9K.a38ViG2' -H "Content-Type:application/json" -d '{"Name":"ocker", "Running":true,"Paused":false,"Restarting":false, "Image": "docker.lab.int:5000/academy:1", "AttachStdin":false, "AttachStdout":false, "Tty":false, "OpenStdin":false, "DisableNetwork":false, "OOMKilled":false,"Dead":false, "Args":["-g","daemon off;"], "Cmd":["nginx","-g","daemon off;"], "PublishAllPorts":true, "RestartPolicy": {"Name": "always"}}' http://10.0.0.132:8080/containers/academy_1/rename?name=academy_old


#curl -X POST -H 'X-Access-Token:user:$2a$10$Pp6bdgNtf0e.9QooaQ5YbOiD0IZmpkBxWCtfR64lQpj9K.a38ViG2' -H "Content-Type:application/json" -d '{"Name":"ocker", "Running":true,"Paused":false,"Restarting":false, "Image": "docker.lab.int:5000/academy:1", "AttachStdin":false, "AttachStdout":false, "Tty":false, "OpenStdin":false, "DisableNetwork":false, "OOMKilled":false,"Dead":false, "Args":["-g","daemon off;"], "Cmd":["nginx","-g","daemon off;"], "PublishAllPorts":true, "RestartPolicy": {"Name": "always"}}' http://10.0.0.132:8080/containers/create?name=academy_1

sleep 10

#curl -X POST -H 'X-Access-Token:user:$2a$10$Pp6bdgNtf0e.9QooaQ5YbOiD0IZmpkBxWCtfR64lQpj9K.a38ViG2' -H "Content-Type:application/json" -d '{"Name":"ocker", "Running":true,"Paused":false,"Restarting":false, "Image": "docker.lab.int:5000/academy:1", "AttachStdin":false, "AttachStdout":false, "Tty":false, "OpenStdin":false, "DisableNetwork":false, "OOMKilled":false,"Dead":false, "Args":["-g","daemon off;"], "Cmd":["nginx","-g","daemon off;"], "PublishAllPorts":true, "RestartPolicy": {"Name": "always"}}' http://10.0.0.132:8080/containers/academy_1/start

##----------------------------------------------------------------


## For Rancher
#export versionBuild=$(curl -s http://10.0.0.135:8500/v1/kv/service/version_Build | sed -e 's/["]/''/g' | awk -F "," '{print $4}' | awk -F ":" '{print $2}' | base64 -d | xargs)
#echo 'version Build - '$versionBuild

#export versionTest=$(curl -s http://10.0.0.135:8500/v1/kv/service/version_Test | sed -e 's/["]/''/g' | awk -F "," '{print $4}' | awk -F ":" '{print $2}' | base64 -d | xargs)
#echo 'version Test - '$versionTest

#export versionStaging=$(curl -s http://10.0.0.135:8500/v1/kv/service/version_Staging | sed -e 's/["]/''/g' | awk -F "," '{print $4}' | awk -F ":" '{print $2}' | base64 -d | xargs)
#echo 'version Staging - '$versionStaging

#export versionBlue=$(curl -s http://10.0.0.135:8500/v1/kv/service/version_Blue | sed -e 's/["]/''/g' | awk -F "," '{print $4}' | awk -F ":" '{print $2}' | base64 -d | xargs)
#echo 'version Blue - '$versionBlue

#export versionGreen=$(curl -s http://10.0.0.135:8500/v1/kv/service/version_Green | sed -e 's/["]/''/g' | awk -F "," '{print $4}' | awk -F ":" '{print $2}' | base64 -d | xargs)
#echo 'version Green - '$versionGreen




### Test deploy
# ./.drone/rancher-compose --url http://rancher.lab.int:8080 --access-key 53783E39C798B5A008B5 --secret-key HX985ocWCUqqzs7q5Nwk2AkMCTtCnDAHUzev9jyf --file docker-compose-test.yml -p Test rm --force
#sleep 20
# ./.drone/rancher-compose --url http://rancher.lab.int:8080 --access-key 53783E39C798B5A008B5 --secret-key HX985ocWCUqqzs7q5Nwk2AkMCTtCnDAHUzev9jyf --file docker-compose-test.yml -p Test up -d

### Staging deploy
# ./.drone/rancher-compose --url http://rancher.lab.int:8080 --access-key 53783E39C798B5A008B5 --secret-key HX985ocWCUqqzs7q5Nwk2AkMCTtCnDAHUzev9jyf --file docker-compose-staging.yml -p Staging rm --force
#sleep 20
# ./.drone/rancher-compose --url http://rancher.lab.int:8080 --access-key 53783E39C798B5A008B5 --secret-key HX985ocWCUqqzs7q5Nwk2AkMCTtCnDAHUzev9jyf --file docker-compose-staging.yml -p Staging up -d

### Stable deploy
# --file docker-compose-prod.yml --file  rancher-compose-prod.yml
# ./.drone/rancher-compose --url http://5a.int:8080 --access-key 53783E39C798B5A008B5 --secret-key HX985ocWCUqqzs7q5Nwk2AkMCTtCnDAHUzev9jyf --file docker-compose-stable.yml -p Stable rm --force
#sleep 20
# ./.drone/rancher-compose --url http://5a.lab.int:8080 --access-key 53783E39C798B5A008B5 --secret-key HX985ocWCUqqzs7q5Nwk2AkMCTtCnDAHUzev9jyf --file docker-compose-stable.yml -p Stable up -d

### Effie-Test deploy
#--file docker-compose-prod.yml --file  rancher-compose-prod.yml
 ./.drone/rancher-compose --url http://kiv-effie-rancher:8080 --access-key AABCDD1B2CF1944D296B --secret-key wrL9nHBNG9AEJ6WuT8iBMfSkbKCERkKiibA4Kikr --file docker-compose-test.yml -p Effie-Test rm --force
sleep 20
 ./.drone/rancher-compose --url http://kiv-effie-rancher:8080 --access-key AABCDD1B2CF1944D296B --secret-key wrL9nHBNG9AEJ6WuT8iBMfSkbKCERkKiibA4Kikr --file docker-compose-test.yml -p Effie-Test up -d

## Deploy to Marathon
# curl -X POST -H "Content-Type: application/json" m-master-01.lab.int:8080/v2/apps -d @wa.json | python -m json.tool
# curl -X POST m-master-01.lab.int:8080/v2/apps/wa/ restart
