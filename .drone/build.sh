#!/bin/bash

set -e  
#cd /var/cache/drone/src/path/to/app

# [pass tests here]

wrapdocker &  
sleep 5

### For debug  #############
echo $PWD
NUMBER=$(git rev-list HEAD --count)
echo 'NUMBER - '$NUMBER

echo $$DRONE_COMMIT
echo $$BUILD_ID
echo $$CI_BUILD_NUMBER
echo $$CI_JOB_NUMBER
###########################


# set versions
#export versionBuild=$(curl -s http://10.0.0.135:8500/v1/kv/service/version_Build | sed -e 's/["]/''/g' | awk -F "," '{print $4}' | awk -F ":" '{print $2}' | base64 -d | xargs)
#echo 'version Build - '$versionBuild

##export versionTest=$(curl -s http://10.0.0.135:8500/v1/kv/service/version_Test | sed -e 's/["]/''/g' | awk -F "," '{print $4}' | awk -F ":" '{print $2}' | base64 -d | xargs)
##echo 'version Test - '$versionTest

##export versionStaging=$(curl -s http://10.0.0.135:8500/v1/kv/service/version_Staging | sed -e 's/["]/''/g' | awk -F "," '{print $4}' | awk -F ":" '{print $2}' | base64 -d | xargs)
##echo 'version Staging - '$versionStaging

### For debug ###########
##export versionBlue=$(curl -s http://10.0.0.135:8500/v1/kv/service/version_Blue | sed -e 's/["]/''/g' | awk -F "," '{print $4}' | awk -F ":" '{print $2}' | base64 -d | xargs)
##echo 'version Blue - '$versionBlue

##export versionGreen=$(curl -s http://10.0.0.135:8500/v1/kv/service/version_Green | sed -e 's/["]/''/g' | awk -F "," '{print $4}' | awk -F ":" '{print $2}' | base64 -d | xargs)
#3echo 'version Green - '$versionGreen
########################



##export DOCKER_HOST=swarm3.lab.int:2375
#docker login --username=docker --password=pocker https://registry.lab.int/
#docker build -t kiv-effie-registry/debian:$versionBuild .

docker push kiv-effie-registry/debian:$versionBuild




